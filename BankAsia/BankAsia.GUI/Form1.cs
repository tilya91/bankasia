﻿using System.Net.Http;

namespace BankAsia.GUI
{
    public partial class Form : System.Windows.Forms.Form
    {
        private readonly HttpClient _client = new HttpClient(); 

        public Form()
        {
            InitializeComponent(); 

            textBoxSearch.TextChanged += (sender, args) =>
            {
                var url = $"http://localhost:49707/api/data/search?name={textBoxSearch.Text}"; 

                var response = _client.GetAsync(url).Result;

                var result = response.Content.ReadAsStringAsync().Result;

                textBoxResult.Text = result;
            };
        }
    } 
}
