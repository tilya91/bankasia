﻿using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using BankAsia.Api.Models;
using Dapper;

namespace BankAsia.Api.Controllers
{
    [RoutePrefix("api/data")]
    public class DataController : ApiController
    {
        private readonly string _connectionString =
            ConfigurationManager.ConnectionStrings["bank-asia"].ConnectionString;

        [HttpGet]
        [Route("search")]
        public IHttpActionResult GetData([FromUri] SearchModel model)
        {
            var name = model.Name;

            using (var connection = new SqlConnection(_connectionString))
            {
                const string sql = "select name from data where name like @name";
                name = $"{name}%";

                var datas = connection.Query(sql, new {name}).Select(d => d.name).ToArray();
                return Ok(datas);
            }
        }
    }
}